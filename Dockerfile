FROM eclipse-temurin:17-jre-alpine
VOLUME /tmp
RUN mkdir /opt/app
COPY target/*.jar /opt/app/product.jar
EXPOSE 9993
CMD [ "java", "-jar", "/opt/app/product.jar"]