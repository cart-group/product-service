package com.gcash.product.service;

import java.util.List;
import java.util.UUID;

import com.gcash.product.dto.PatchProductRequest;
import com.gcash.product.dto.ProductRequest;
import com.gcash.product.dto.SearchProductRequest;
import com.gcash.product.entity.Product;

public interface ProductService {

	Product create(ProductRequest request);
	Product update(UUID productId, ProductRequest request);
	Product patch(UUID productId, PatchProductRequest request);
	Product get(UUID productid);
	List<Product> getAll(SearchProductRequest criteria);
}
