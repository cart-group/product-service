package com.gcash.product.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.camel.Produce;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.gcash.product.dto.ActivityLogRequest;
import com.gcash.product.dto.InventoryRequest;
import com.gcash.product.entity.Inventory;
import com.gcash.product.enums.ActivityLogMethodsEnum;
import com.gcash.product.exceptions.InventoryNotFoundException;
import com.gcash.product.repository.InventoryRepository;
import com.gcash.product.service.ActivityLogService;
import com.gcash.product.service.InventoryService;
import com.gcash.product.service.ProductService;
import com.gcash.product.util.ActivityLogUtil;

import jakarta.transaction.Transactional;

@Service("inventoryService")
public class InventoryImplService implements InventoryService {
	
	@Autowired
	private InventoryRepository inventoryRepository;
	
	@Autowired
	private ProductService productService;
	
	@Produce("direct:startActivityLog")
	private ActivityLogService activityLogService;

	@Transactional
	@Override
	public Inventory add(UUID productId, InventoryRequest request) {
		ActivityLogRequest activityLog =  ActivityLogUtil.generateRequestActivityLog(
				ActivityLogMethodsEnum.POST.toString(), 
				UUID.randomUUID().toString());
		activityLogService.log(activityLog);
		
		// Validate that the product exists
		productService.get(productId);
		
		Inventory inventory = null;
		try {
			inventory = getByProductId(productId);
			BeanUtils.copyProperties(request, inventory, "id", "available");
			int newTotalAvailable = inventory.getAvailable() + request.getQuantity();
			inventory.setAvailable(newTotalAvailable);
		} catch(InventoryNotFoundException e) {
			inventory = new Inventory();
			inventory.setAvailable(request.getQuantity());
			inventory.setProductId(productId);
		}
		
		return inventoryRepository.save(inventory);
	}

	@Override
	public Inventory get(UUID inventoryId) {
		Optional<Inventory> inventoryOpt = inventoryRepository.findById(inventoryId);
		if (inventoryOpt.isPresent()) {
			return inventoryOpt.get();
		}
		
		throw new InventoryNotFoundException(String.format("Inventory not found: %s", inventoryId));
	}

	@Override
	public List<Inventory> getAll() {
		ActivityLogRequest activityLog =  ActivityLogUtil.generateRequestActivityLog(
				ActivityLogMethodsEnum.GET.toString(), 
				UUID.randomUUID().toString());
		activityLogService.log(activityLog);
		return inventoryRepository.findAll();
	}

	@Override
	public Inventory getByProductId(UUID productId) {
		Inventory inventory = inventoryRepository.findByProductId(productId);
		if (!ObjectUtils.isEmpty(inventory)) {
			return inventory;
		}
		
		throw new InventoryNotFoundException(String.format("Inventory not found by product id: %s", productId));
	}

	@Override
	public List<Inventory> getByProductIds(List<UUID> productIds) {
		ActivityLogRequest activityLog =  ActivityLogUtil.generateRequestActivityLog(
				ActivityLogMethodsEnum.GET.toString(), 
				UUID.randomUUID().toString());
		activityLogService.log(activityLog);
		return inventoryRepository.findByProductIdIn(productIds);
	}

	@Override
	public List<Inventory> updateInventory(List<Inventory> inventories) {
		ActivityLogRequest activityLog =  ActivityLogUtil.generateRequestActivityLog(
				ActivityLogMethodsEnum.POST.toString(), 
				UUID.randomUUID().toString());
		activityLogService.log(activityLog);
		return inventoryRepository.saveAll(inventories);
	}

	@Override
	public boolean isAvailability(UUID productId, int quantity) {
		boolean available = false;
		Inventory inventory = getByProductId(productId);
		if (quantity <= inventory.getAvailable()) {
			available = true;
		}
		
		return available;
	}

}
