package com.gcash.product.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import org.apache.camel.Produce;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.gcash.product.dto.ActivityLogRequest;
import com.gcash.product.dto.PatchProductRequest;
import com.gcash.product.dto.ProductRequest;
import com.gcash.product.dto.SearchProductRequest;
import com.gcash.product.dto.SearchProductCriteriaRequest;
import com.gcash.product.entity.Product;
import com.gcash.product.enums.ActivityLogMethodsEnum;
import com.gcash.product.exceptions.ProductNoNewDetailsException;
import com.gcash.product.exceptions.ProductNotFoundException;
import com.gcash.product.repository.ProductRepository;
import com.gcash.product.service.ActivityLogService;
import com.gcash.product.service.ProductService;
import com.gcash.product.util.ActivityLogUtil;

@Service("productSevice")
public class ProductImplSevice implements ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Produce("direct:startActivityLog")
	private ActivityLogService activityLogService;

	@Override
	public Product create(ProductRequest request) {
		Product product = new Product();
		BeanUtils.copyProperties(request, product);
		
		ActivityLogRequest activityLog =  ActivityLogUtil.generateRequestActivityLog(
				ActivityLogMethodsEnum.POST.toString(), 
				UUID.randomUUID().toString());
		activityLogService.log(activityLog);
		
		return productRepository.save(product);
	}

	@Override
	public Product update(UUID productId, ProductRequest request) {
		Product product = get(productId);
		BeanUtils.copyProperties(request, product, "id");
		
		ActivityLogRequest activityLog =  ActivityLogUtil.generateRequestActivityLog(
				ActivityLogMethodsEnum.PUT.toString(), 
				UUID.randomUUID().toString());
		activityLogService.log(activityLog);
		
		return productRepository.save(product);
	}

	@Override
	public Product get(UUID productId) {
		Optional<Product> productOpt = productRepository.findById(productId);
		if (productOpt.isPresent()) {
			return productOpt.get();
		}
		
		throw new ProductNotFoundException(String.format("Product not found: %s", productId));
	}

	@Override
	public List<Product> getAll(SearchProductRequest criteria) {
		ActivityLogRequest activityLog =  ActivityLogUtil.generateRequestActivityLog(
				ActivityLogMethodsEnum.GET.toString(), 
				UUID.randomUUID().toString());
		activityLogService.log(activityLog);
		
		if (!Stream.of(criteria).allMatch(Objects::isNull)) {
			Product product = new Product();
			BeanUtils.copyProperties(criteria, product);
			Example<Product> example = Example.of(product, SearchProductCriteriaRequest.SEARCH_CONDITIONS_MATCH_ANY);
			
			return productRepository.findAll(example);
		} else {
			return productRepository.findAll();
		}
	}

	@Override
	public Product patch(UUID productId, PatchProductRequest request) {
		Product product = get(productId);
		if (changeIfPresent(request, product)) {
			ActivityLogRequest activityLog =  ActivityLogUtil.generateRequestActivityLog(
					ActivityLogMethodsEnum.PATCH.toString(), 
					UUID.randomUUID().toString());
			activityLogService.log(activityLog);
			
			return productRepository.save(product);
		}
		
		throw new ProductNoNewDetailsException(String.format("No new Product details to update: %s", productId));
	}
	
	private boolean changeIfPresent(PatchProductRequest request, Product product) {
		boolean hasNewValue = false;
		if (Stream.of(request.getCode(), request.getDescription(), request.getName()).allMatch(Objects::isNull)) {
			return hasNewValue;
		}
		
		hasNewValue = true;
		
		if (StringUtils.hasText(request.getCode())) {
			product.setCode(request.getCode());
		}
		
		if (StringUtils.hasText(request.getDescription())) {
			product.setDescription(request.getDescription());
		}

		if (StringUtils.hasText(request.getName())) {
			product.setName(request.getName());
		}
		
		return hasNewValue;
	}
}
