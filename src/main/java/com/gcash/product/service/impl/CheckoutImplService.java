package com.gcash.product.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.camel.Produce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import com.gcash.product.dto.ActivityLogRequest;
import com.gcash.product.dto.CheckoutRequest;
import com.gcash.product.entity.Checkout;
import com.gcash.product.entity.Inventory;
import com.gcash.product.enums.ActivityLogMethodsEnum;
import com.gcash.product.exceptions.CheckoutNotFoundException;
import com.gcash.product.exceptions.InsufficientInventoryException;
import com.gcash.product.repository.CheckoutRepository;
import com.gcash.product.service.ActivityLogService;
import com.gcash.product.service.CheckoutService;
import com.gcash.product.service.InventoryService;
import com.gcash.product.util.ActivityLogUtil;

@Service
public class CheckoutImplService implements CheckoutService {
	
	@Autowired
	private InventoryService inventoryService;
	
	@Autowired
	private CheckoutRepository checkoutRepository;
	
	@Produce("direct:startActivityLog")
	private ActivityLogService activityLogService;

	@Transactional
	@Override
	public Checkout checkout(List<CheckoutRequest> checkoutOrders) {
		ActivityLogRequest activityLog =  ActivityLogUtil.generateRequestActivityLog(
				ActivityLogMethodsEnum.POST.toString(), 
				UUID.randomUUID().toString());
		activityLogService.log(activityLog);
		
		// Validate Availability
		List<Inventory> inventories = inventoryService.getByProductIds(checkoutOrders.stream()
				.map(CheckoutRequest::getProductId)
				.toList());
		Map<UUID, Inventory> inventoryMap = inventories.stream()
				.collect(Collectors.toMap(Inventory::getProductId, Function.identity()));
		List<Inventory> toUpdate = new ArrayList<>();
		validateAvailability(checkoutOrders, inventoryMap, toUpdate);
		
		// Update Inventory
		inventoryService.updateInventory(inventories);
		
		// Save Checkout Info
		Checkout checkout = new Checkout();
		checkout.setCheckoutOrders(checkoutOrders);
		checkout.setLoggedTime(LocalDateTime.now());
		return checkoutRepository.save(checkout);
	}
	
	private void validateAvailability(List<CheckoutRequest> checkoutOrders, Map<UUID, Inventory> inventoryMap, List<Inventory> toUpdate) {
		List<UUID> insufficientStocks = new ArrayList<>();
		
		ActivityLogRequest activityLog =  ActivityLogUtil.generateRequestActivityLog(
				ActivityLogMethodsEnum.GET.toString(), 
				UUID.randomUUID().toString());
		activityLogService.log(activityLog);
		
		for (CheckoutRequest checkoutRequest : checkoutOrders) {
			UUID productId = checkoutRequest.getProductId();
			Inventory inventory = inventoryMap.get(productId);
			if (ObjectUtils.isEmpty(inventory)
					|| inventory.getAvailable() < checkoutRequest.getQuantity()) {
				insufficientStocks.add(productId);
			} else {
				inventory.setAvailable(inventory.getAvailable() - checkoutRequest.getQuantity());
				toUpdate.add(inventory);
			}
		}
		
		if (!insufficientStocks.isEmpty()) {
			throw new InsufficientInventoryException("Insufficient inventory for the following products: " + Arrays.toString(insufficientStocks.toArray()));
		}
	}

	@Override
	public Checkout viewCheckout(UUID checkoutId) {
		Optional<Checkout> checkoutOpt = checkoutRepository.findById(checkoutId);
		if (checkoutOpt.isPresent()) {
			return checkoutOpt.get();
		}
		
		throw new CheckoutNotFoundException(String.format("Checkout not found: %s", checkoutId));
	}
}
