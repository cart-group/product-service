package com.gcash.product.service;

import com.gcash.product.dto.ActivityLogRequest;

public interface ActivityLogService {
	void log(ActivityLogRequest activityLog);
}
