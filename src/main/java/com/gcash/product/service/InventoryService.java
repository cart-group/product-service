package com.gcash.product.service;

import java.util.List;
import java.util.UUID;

import com.gcash.product.dto.InventoryRequest;
import com.gcash.product.entity.Inventory;

public interface InventoryService {
	Inventory add(UUID productId, InventoryRequest request);
	Inventory get(UUID inventoryId);
	Inventory getByProductId(UUID productId);
	List<Inventory> updateInventory(List<Inventory> inventories);
	List<Inventory> getAll();
	List<Inventory> getByProductIds(List<UUID> productIds);
	
	boolean isAvailability(UUID productId, int quantity);
}
