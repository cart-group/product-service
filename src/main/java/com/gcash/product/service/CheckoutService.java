package com.gcash.product.service;

import java.util.List;
import java.util.UUID;

import com.gcash.product.dto.CheckoutRequest;
import com.gcash.product.entity.Checkout;

public interface CheckoutService {
	Checkout checkout(List<CheckoutRequest> checkoutOrders);
	Checkout viewCheckout(UUID checkoutId);
}
