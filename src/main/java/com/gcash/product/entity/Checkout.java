package com.gcash.product.entity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import com.gcash.product.dto.CheckoutRequest;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity
public class Checkout {
	@Id
	@GeneratedValue(strategy = GenerationType.UUID)
	private UUID id;
	
	@JdbcTypeCode(SqlTypes.JSON)
	private List<CheckoutRequest> checkoutOrders;
	
	private LocalDateTime loggedTime;
}
