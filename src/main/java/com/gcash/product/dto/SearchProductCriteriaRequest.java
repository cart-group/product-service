package com.gcash.product.dto;

import org.springframework.data.domain.ExampleMatcher;

public class SearchProductCriteriaRequest {
	private SearchProductCriteriaRequest() {}
	
	public static final ExampleMatcher SEARCH_CONDITIONS_MATCH_ANY = ExampleMatcher.matchingAny()
			.withMatcher("id", ExampleMatcher.GenericPropertyMatchers.exact().ignoreCase())
			.withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
			.withMatcher("description", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
			.withMatcher("code", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase());
}
