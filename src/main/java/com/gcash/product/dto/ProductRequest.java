package com.gcash.product.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
@Valid
public class ProductRequest {
	
	@Schema(description = "Name of the product", example = "John Doemeng Brand")
	@Size(min = 3, max = 200)
	private String name;
	
	@Schema(description = "Description of the product", example = "The number 1 brand in the whole universe")
	@Size(min = 3, max = 255)
	private String description;
	
	@Schema(description = "unique identifier/SKU of the product", example = "JDB0021")
	@Size(min = 3, max = 10)
	private String code;
}
