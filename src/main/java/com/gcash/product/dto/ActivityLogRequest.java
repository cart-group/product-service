package com.gcash.product.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActivityLogRequest {
	private String method;
	
	private String url;
	
	private String transactionReferenceNumber;
	
	private LocalDateTime loggedTime;
}
