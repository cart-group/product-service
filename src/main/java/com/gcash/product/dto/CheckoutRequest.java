package com.gcash.product.dto;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.validation.constraints.Positive;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckoutRequest {
	private UUID productId;
	
	@Positive(message = "quantity must be greater than 0")
	private int quantity;
}
