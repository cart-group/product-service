package com.gcash.product.dto;

import jakarta.annotation.Nullable;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
@Valid
public class PatchProductRequest {
	
	@Nullable
	@Size(min = 3, max = 200)
	private String name;
	
	@Nullable
	@Size(min = 3, max = 255)
	private String description;
	
	@Nullable
	@Size(min = 3, max = 10)
	private String code;
}
