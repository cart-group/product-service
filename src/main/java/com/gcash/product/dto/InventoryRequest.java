package com.gcash.product.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Positive;
import lombok.Data;

@Data
@Valid
public class InventoryRequest {
	@Positive(message = "quantity must be greater than 0")
	private int quantity;
}
