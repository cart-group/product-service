package com.gcash.product.exceptions;

public class InventoryNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 5666716857342799685L;

	public InventoryNotFoundException(String message) {
		super(message);
	}
}
