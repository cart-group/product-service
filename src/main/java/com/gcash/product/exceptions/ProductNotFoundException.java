package com.gcash.product.exceptions;

public class ProductNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 5666716857342799685L;

	public ProductNotFoundException(String message) {
		super(message);
	}
}
