package com.gcash.product.exceptions;

public class InsufficientInventoryException extends RuntimeException {
	private static final long serialVersionUID = 5666716857342799685L;

	public InsufficientInventoryException(String message) {
		super(message);
	}
}
