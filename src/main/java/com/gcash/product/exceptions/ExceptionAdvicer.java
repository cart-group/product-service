package com.gcash.product.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;

@RestControllerAdvice
public class ExceptionAdvicer {

	@ExceptionHandler(ProductNotFoundException.class)
	public ResponseEntity<ErrorResponse> handleProductNotFoundException(ProductNotFoundException e) {
		ErrorResponse response = new ErrorResponse();
		response.setError(e.getMessage());
		response.setCode("PR404");
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}
	
	@ExceptionHandler(InventoryNotFoundException.class)
	public ResponseEntity<ErrorResponse> handleInventoryNotFoundException(InventoryNotFoundException e) {
		ErrorResponse response = new ErrorResponse();
		response.setError(e.getMessage());
		response.setCode("INV404");
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}
	
	@ExceptionHandler(ProductNoNewDetailsException.class)
	public ResponseEntity<ErrorResponse> handleProductNoNewDetailsException(ProductNoNewDetailsException e) {
		ErrorResponse response = new ErrorResponse();
		response.setError(e.getMessage());
		response.setCode("PR4ND");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}

	@ExceptionHandler(CheckoutNotFoundException.class)
	public ResponseEntity<ErrorResponse> handleCheckoutNotFoundException(CheckoutNotFoundException e) {
		ErrorResponse response = new ErrorResponse();
		response.setError(e.getMessage());
		response.setCode("CO404");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
	
	@ExceptionHandler(InsufficientInventoryException.class)
	public ResponseEntity<ErrorResponse> handleInsufficientInventoryException(InsufficientInventoryException e) {
		ErrorResponse response = new ErrorResponse();
		response.setError(e.getMessage());
		response.setCode("INVXF");
		return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(response);
	}
	
	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<ErrorResponse> handleConstraintViolationException(ConstraintViolationException e) {
		ErrorResponse response = new ErrorResponse();
		response.setErrors(e.getConstraintViolations().stream().map(ConstraintViolation::getMessage).toList());
		response.setCode("PRODVLDE");
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> handleException(Exception e) {
		ErrorResponse response = new ErrorResponse();
		response.setError(e.getMessage());
		response.setCode("INTX");
		
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
	}
}
