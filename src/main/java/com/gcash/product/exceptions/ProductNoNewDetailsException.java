package com.gcash.product.exceptions;

public class ProductNoNewDetailsException extends RuntimeException {
	private static final long serialVersionUID = 5666716857342799685L;

	public ProductNoNewDetailsException(String message) {
		super(message);
	}
}
