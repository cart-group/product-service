package com.gcash.product.exceptions;

public class CheckoutNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 5666716857342799685L;

	public CheckoutNotFoundException(String message) {
		super(message);
	}
}
