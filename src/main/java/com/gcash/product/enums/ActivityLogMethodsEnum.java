package com.gcash.product.enums;

public enum ActivityLogMethodsEnum {
	POST,
	PUT,
	PATCH,
	GET
}
