package com.gcash.product.message.producer;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.gcash.product.dto.ActivityLogRequest;

@Component
public class ActivityLogProducer extends RouteBuilder {
	
	@Value("${kafka.broker.host}")
	private String brokerHost;
	
	@Value("${kafka.broker.port}")
	private int brokerPort;
	
	private static final String KAFKA_URL = "kafka:product-activity?brokers=%s:%s";

	@Override
	public void configure() throws Exception {
		JacksonDataFormat jsonDataFormat = new JacksonDataFormat(ActivityLogRequest.class);
		jsonDataFormat.setObjectMapper(new ObjectMapper().registerModule(new JavaTimeModule()));
		
		from("direct:startActivityLog")
//			.unmarshal()
//			.json(JsonLibrary.Jackson, Receipt.class)
//			.process(receiptToMovieSlotNotificationProcessor)
			.marshal(jsonDataFormat)
			.to(getKafkaUrl());
		
	}
	
	private String getKafkaUrl() {
		return String.format(KAFKA_URL, brokerHost, brokerPort);
	}

}
