package com.gcash.product.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcash.product.dto.CheckoutRequest;
import com.gcash.product.entity.Checkout;
import com.gcash.product.service.CheckoutService;

@RestController
@RequestMapping("checkout")
public class CheckoutController {
	
	@Autowired
	private CheckoutService checkoutService;

	@PostMapping
	public ResponseEntity<Checkout> checkout(@RequestBody List<CheckoutRequest> checkoutOrders) {
		return ResponseEntity.ok(checkoutService.checkout(checkoutOrders));
	}
}
