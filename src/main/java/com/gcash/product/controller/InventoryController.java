package com.gcash.product.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcash.product.dto.InventoryRequest;
import com.gcash.product.entity.Inventory;
import com.gcash.product.service.InventoryService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

@RestController
@RequestMapping
public class InventoryController {
	
	@Autowired
	private InventoryService inventoryService;
	
	@GetMapping("inventories")
	public ResponseEntity<List<Inventory>> getInventories() {
		return ResponseEntity.ok(inventoryService.getAll());
	}
	
	@GetMapping("/inventories/{inventoryId}")
	public ResponseEntity<Inventory> getInventories(@PathVariable @NotNull UUID inventoryId) {
		return ResponseEntity.ok(inventoryService.get(inventoryId));
	}
	
	@GetMapping("/products/{productId}/inventories")
	public ResponseEntity<Inventory> getInventoriesByProductId(@PathVariable @NotNull UUID productId) {
		return ResponseEntity.ok(inventoryService.getByProductId(productId));
	}
	
	@PostMapping("/products/{productId}/inventories")
	public ResponseEntity<Inventory> addInventory(@PathVariable @NotNull UUID productId,
			@RequestBody @Valid InventoryRequest request) {
		return ResponseEntity.ok(inventoryService.add(productId, request));
	}
	
	@PostMapping("/products/{productId}/inventories/verification")
	public ResponseEntity<Boolean> isAvailable(@PathVariable @NotNull UUID productId,
			@RequestBody @Valid InventoryRequest request) {
		return ResponseEntity.ok(inventoryService.isAvailability(productId, request.getQuantity()));
	}
}
