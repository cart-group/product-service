package com.gcash.product.controller;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.gcash.product.dto.PatchProductRequest;
import com.gcash.product.dto.ProductRequest;
import com.gcash.product.dto.SearchProductRequest;
import com.gcash.product.entity.Product;
import com.gcash.product.service.ProductService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

@RestController
@RequestMapping("products")
@Validated
public class ProductController {
	@Autowired
	private ProductService productService;
	
	@Operation(summary = "Retrieve products by search criteria")
	@ApiResponses(value = {
	        @ApiResponse(responseCode = "200", description = "successful retrieval", content = @Content(array = @ArraySchema(schema = @Schema(implementation = Product.class)))),
	        @ApiResponse(responseCode = "200", description = "empty retrieval")})
	@GetMapping
	public ResponseEntity<List<Product>> getProducts(@RequestParam(name = "id", required = false) UUID id,
			@RequestParam(name = "name", required = false) String name,
			@RequestParam(name = "description", required = false) String description,
			@RequestParam(name = "code", required = false) String code) {
		SearchProductRequest criteria = SearchProductRequest.builder()
				.id(id)
				.name(name)
				.description(description)
				.code(code)
				.build();
		return ResponseEntity.ok(productService.getAll(criteria));
	}
	
	@Operation(summary = "Retrieve product by id")
	@ApiResponses(value = {
	        @ApiResponse(responseCode = "200", description = "successful retrieval", content = @Content(schema = @Schema(implementation = Product.class))),
	        @ApiResponse(responseCode = "404", description = "Product not found")})
	@GetMapping("/{productId}")
	public ResponseEntity<Product> getProduct(@PathVariable @NotNull UUID productId) {
		return ResponseEntity.ok(productService.get(productId));
	}
	
	@Operation(summary = "Create product")
	@ApiResponses(value = {
	        @ApiResponse(responseCode = "201", description = "successful retrieval", content = @Content(schema = @Schema(implementation = Product.class)))
	})
	@PostMapping
	public ResponseEntity<Product> createProduct(@RequestBody @Valid ProductRequest request) {
		Product product = productService.create(request);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
			.path("/{id}")
			.buildAndExpand(product.getId())
			.toUri();
		
		return ResponseEntity.created(uri).body(product);
	}
	
	@Operation(summary = "Update product")
	@PutMapping("/{productId}")
	public ResponseEntity<Product> updateProduct(@PathVariable @NotNull UUID productId,
			@RequestBody @Valid ProductRequest request) {
		return ResponseEntity.ok(productService.update(productId, request));
	}
	
	@Operation(summary = "Patch product")
	@PatchMapping("/{productId}")
	public ResponseEntity<Product> patchProduct(@PathVariable @NotNull UUID productId,
			@RequestBody @Valid PatchProductRequest request) {
		return ResponseEntity.ok(productService.patch(productId, request));
	}
}
