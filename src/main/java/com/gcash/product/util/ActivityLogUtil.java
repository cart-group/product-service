package com.gcash.product.util;

import java.time.LocalDateTime;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.gcash.product.dto.ActivityLogRequest;

public class ActivityLogUtil {
	private ActivityLogUtil() {}
	
	public static ActivityLogRequest generateRequestActivityLog(String method, String trn) {
		String url = ServletUriComponentsBuilder.fromCurrentRequest().toUriString();
		
		ActivityLogRequest requestActivityLog = new ActivityLogRequest();
		requestActivityLog.setLoggedTime(LocalDateTime.now());
		requestActivityLog.setMethod(method);
		requestActivityLog.setUrl(url);
		requestActivityLog.setTransactionReferenceNumber(trn);
		
		return requestActivityLog;
	}
	
}
