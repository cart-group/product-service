package com.gcash.product.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gcash.product.entity.Inventory;

public interface InventoryRepository extends JpaRepository<Inventory, UUID> {

	Inventory findByProductId(UUID productId);
	
	List<Inventory> findByProductIdIn(List<UUID> productId);
}
