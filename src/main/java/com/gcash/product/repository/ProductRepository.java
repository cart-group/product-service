package com.gcash.product.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gcash.product.entity.Product;

public interface ProductRepository extends JpaRepository<Product, UUID> {

}
