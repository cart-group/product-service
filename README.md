# product-service



## Docker Image
Docker Repository:

```
https://hub.docker.com/r/villetorio/product-service
```

Docker pull

```
docker pull villetorio/product-service
```
## Run via docker compose
```
version: '3.1'
services:
  product-service:
    build: .
    ports:
      - "9993:9993"
    environment:
      kafka.broker.host: "kafka"
      kafka.broker.port: "9092"
      spring.datasource.url: "jdbc:mysql://db:3306/products?createDatabaseIfNotExists=true&useSSL=false&allowPublicKeyRetrieval=true&autoReconnect=true"
      spring.datasource.username: "product_user"
      spring.datasource.password: "pass"

networks: 
  default: 
    external: 
      name: external-bridge
  
        
```

## K8s Deployment/Service
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: product
spec:
  selector:
    matchLabels:
      app: product
  template:
    metadata:
      labels:
        app: product
    spec:
      containers:
      - name: product
        image: villetorio/product-service:1.0.0
        livenessProbe:
          httpGet:
            path: "/actuator/health/liveness"
            port: 9993
          initialDelaySeconds: 60
          periodSeconds: 10
        readinessProbe:
          httpGet:
            path: "/actuator/health/readiness"
            port: 9993
          initialDelaySeconds: 60
          periodSeconds: 10
        resources:
          limits:
            memory: "512Mi"
            cpu: "500m"
        ports:
        - containerPort: 9993
        env:
          - name: kafka.broker.host
            value: "kafka-svc"
          - name: kafka.broker.port
            value: "9092"
          - name: spring.datasource.url
            value: "jdbc:mysql://mysqldb:3306/products?createDatabaseIfNotExists=true&useSSL=false&allowPublicKeyRetrieval=true&autoReconnect=true"
          - name: spring.datasource.username
            value: "product_user"
          - name: spring.datasource.password
            value: "pass"
---
apiVersion: v1
kind: Service
metadata:
  name: product-service
spec:
  type: LoadBalancer
  selector:
    app: product
  ports:
  - port: 9993
    targetPort: 9993
```
